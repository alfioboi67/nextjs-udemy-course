import MeetupList from "../components/meetups/MeetupList";
import {MongoClient} from "mongodb";
import {Fragment} from "react";
import Head from "next/head";
function Homepage (props) {
    return (<Fragment>
        <Head>
            <title>Homepage</title>
            <meta name="description" content="This is a simply description for the homepage"/>
        </Head>
        <MeetupList meetups={props.meetups}/>
    </Fragment>)
}
export async function getStaticProps() {
    const client = MongoClient.connect('mongodb+srv://alfioboi:71nBaJZuGHCyZMuD@cluster0.rn3r2cd.mongodb.net/?retryWrites=true&w=majority');
    const db = (await client).db()
    const meetupsCollection = db.collection('meetups');
    const meetupList = await  meetupsCollection.find().toArray();
    (await client).close();
    return {
        props: {
            meetups: meetupList.map((meetup) => ({
                id: meetup._id.toString(),
                image: meetup.image,
                address: meetup.address,
                description: meetup.description
            }))
        },
        revalidate: 1
    }
}
/*export async function getServerSideProps(context) {
    const req = context.req;
    const res = context.res;
    return  {
        props: {
            meetups: DUMMY_MEETUPS
        }
    }
}*/
export default Homepage;
