import MeetupDetailComponent from "../../components/meetups/MeetupDetail";
import {MongoClient, ObjectId} from "mongodb";
import {Fragment} from "react";
import Head from "next/head";

function MeetupDetail(props) {
  return (
        <Fragment>
            <Head>
                <title>{props.meetupData.title}</title>
                <meta name="description" content={props.meetupData.description}/>
            </Head>
            <MeetupDetailComponent meetup={props.meetupData}/>
        </Fragment>
    )
}

export async function getStaticPaths() {
    const client = MongoClient.connect('mongodb+srv://alfioboi:71nBaJZuGHCyZMuD@cluster0.rn3r2cd.mongodb.net/?retryWrites=true&w=majority');
    const db = (await client).db()
    const meetupsCollection = db.collection('meetups');
    const meetupList = await  meetupsCollection.find().toArray();
    (await client).close();
    return {
        fallback: 'blocking',
        paths: meetupList.map(meetup => ({
            params: {
                meetupId: meetup._id.toString()
            }
        }))
    }
}

export async function getStaticProps(context) {
    const meetupId = context.params.meetupId;
    const client = MongoClient.connect('mongodb+srv://alfioboi:71nBaJZuGHCyZMuD@cluster0.rn3r2cd.mongodb.net/?retryWrites=true&w=majority');
    const db = (await client).db()
    const meetupsCollection = db.collection('meetups');
    const meetupRetrievedData = await  meetupsCollection.findOne({_id: ObjectId(meetupId)});
    (await client).close();

    return {
        props: {
            meetupData: {
                id: meetupId,
                title: meetupRetrievedData.title ,
                address: meetupRetrievedData.address,
                image: meetupRetrievedData.image,
                description: meetupRetrievedData.description
            }
        },
        revalidate: 1
    }
}

export default MeetupDetail;
